<?php

$distros = array();

$ubuntu = [ "base" => "debian", "archs" => "x64", "Free" => true];
$fedora = [ "base" => "red hat", "archs" => "x64, i386", "Free" => true];
$elementary = [ "base" => "ubuntu", "archs" => "x64", "Free" => false];

//Construim array associatiu
$distros['Ubuntu'] = $ubuntu;
$distros['Fedora'] = $fedora;
$distros['Elementary OS'] = $elementary;

$distros_alt = array();

$ubuntu2 = [ "name" => "Ubuntu", "base" => "debian", "archs" => "x64", "Free" => true];
$fedora2 = [ "name" => "Fedora", "base" => "red hat", "archs" => "x64, i386", "Free" => true];
$elementary2 = [ "name" => "Elemantary", "base" => "ubuntu", "archs" => "x64", "Free" => false];

//Construim array per posició
array_push($distros_alt, $ubuntu2, $fedora2, $elementary2);
