<?php

$distros = array();

$ubuntu = [ "base" => "debian", 
    "archs" => "x64", 
    "Free" => true, 
    "img" => "ubuntu.png"];
$fedora = [ "base" => "red hat", 
    "archs" => "x64, i386", 
    "Free" => true, 
    "img" => "fedora.png"];
$elementary = [ "base" => "ubuntu", 
    "archs" => "x64", 
    "Free" => false, 
    "img" => "elementary.png"];

//Construim array associatiu
$distros['Ubuntu'] = $ubuntu;
$distros['Fedora'] = $fedora;
$distros['Elementary OS'] = $elementary;

$distros_alt = array();

$ubuntu2 = [ "name" => "Ubuntu", 
    "base" => "debian", 
    "archs" => "x64", 
    "Free" => true,
    "img" => "ubuntu.png"];
$fedora2 = [ "name" => "Fedora", 
    "base" => "red hat", 
    "archs" => "x64, i386", 
    "Free" => true,
    "img" => "fedora.png"];
$elementary2 = [ "name" => "Elemantary", 
    "base" => "ubuntu", 
    "archs" => "x64", 
    "Free" => false,
    "img" => "elementary.png"];

//Construim array per posició
array_push($distros_alt, $ubuntu2, $fedora2, $elementary2);

