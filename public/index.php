<?php
require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/dades/distros.php');

$cars = array();
$cars[] = "Panda";
$cars[] = "Tesla";
array_push($cars,"BMW");

$prices = array();
$prices['Tesla'] = 100000;
$prices['Panda'] = 500;
$prices['BMW'] = 80000;

$title = 'Pàgina inicial';

?><html>
  <?php /* Si no el troba -> WARNING */
    include(__DIR__.'/../app/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1>It's working!</h1>
      <?php /* 
      <div style="width:20%;margin:0 auto;">
        <img src="/img/patricio.png" width="100" />
        <video width="640" height="360" controls>
          <source src="<?=RES_FOLDER?>/videos/keyboard.mp4" type="video/mp4">
        </video>
        <iframe src="https://player.vimeo.com/video/317243347" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
      </div> */
      ?>
      <h2>Distros (array associatiu)</h2>
      <table>
        <tr>
          <th>&nbsp;</th>
          <th>Nom</th>
          <th>Base</th>
          <th>Arquitectura</th>
          <th>SW Lliure</th>
        </tr>
        <?php foreach($distros as $name => $data){ ?>
        <tr>
          <td><img src="<?=RES_FOLDER?>/img/<?=$data['img']?>" width="70"/></td>
          <td><?=$name?></td>
          <td><?=$data['base']?></td>
          <td><?=$data['archs']?></td>
          <td><?=$data['Free']?></td>
        </tr>
        <?php } ?>
      </table>
      <hr/>
      <h2>Distros (array per posició)</h2>
      <table>
        <tr>
          <th>&nbsp;</th>
          <th>Nom</th>
          <th>Base</th>
          <th>Arquitectura</th>
          <th>SW Lliure</th>
        </tr>
        <?php //foreach($distros_alt as $data){ 
          for($i = 0 ; $i < count($distros_alt) ; $i++){?>
        <tr>
        <?php /*
          <td><?=$data['name']?></td>
          <td><?=$data['base']?></td>
          <td><?=$data['archs']?></td>
          <td><?=$data['Free']?></td>
        */ ?>
          <td><img src="<?=RES_FOLDER?>/img/<?=$distros_alt[$i]['img']?>" width="70"/></td>
          <td><?=$distros_alt[$i]['name']?></td>
          <td><?=$distros_alt[$i]['base']?></td>
          <td><?=$distros_alt[$i]['archs']?></td>
          <td><?=$distros_alt[$i]['Free']?></td>
        </tr>
        <?php } ?>
      </table>
      <h2>Cotxes</h2>
      <ul>
      <?php for($i = 0 ; $i < count($cars) ; $i++) { ?>
          <li><?php echo $cars[$i]; ?>:<?php echo $prices[$cars[$i]]; ?></li>
      <?php } ?>
      <?php foreach($prices as $marca => $preu){ ?>
        <li><?=$marca?>:<?=$preu?></li>
      <?php } ?>
      </ul>
      <div>Div sample</div>
    </div>
  </body>
</html>